#Webhooks test for the Sqreen 

This is a refernce implementation of the technical task assigned for the Sqreen interview process.

## Task 1
Configure the webhooks for theSqreen and check if the signature is correct for the POST request sent by Sqreen

## Task 2
The recived events are dispatched to further sinks/targets, e.g. logging DB, Slack, Email, etc.

## How to install
```npm install``` would do the trick, all the dependencies are described in the package.json file. Note that the configuration files uses the NodeJS environment variables to determine the port addresse, baseurl, etc. Please look at the ```config.js``` in ```src``` folder for more information. Furthremore, a docker file is also provided to use the containerise-based development. 


## How to Test
```npm test``` uses the Mocha and Chai framework to test the authentication, logging DB, Slack message construction and POST method authentication. All the test are avaialble in the test folder.


## How to Run
```npm start``` would start the server and any events coming from the webhooks will be checked and dispathed to the targets. However, the question is how to expose your endpoint such that Sqreen can send the notifications. In this context, I have used [ngrok](www.ngrok.com) framework for testing purpose. Please look at the [ngrok](www.ngrok.com) webpage to see how local implementation can be exposed over the public network. The link provided my the [ngrok](www.ngrok.com) will be used as a webhook in Sqreen. The logging of all the events and errors will be available at ```module/logging```.


#Detailed Description
The app.js file uses the express framework to recive the POST requests from Sqreen. These reqeuest are passed to the /custom/notification.js interface. From thier two targets (log.js and Slack.js) are used to utilise the incoming messages. 

For logging, I used a simple refernce implementaion of MongoDB called [TingoDb](https://github.com/sergeyksv/tingodb/tree/master/test). The reasons are as follows: it requires minimal configuration for testing; it uses the same functions as of MongoDB, hence can be replace with the MongoDB for production version. 

For Slack, I used the Slack API to send the messages to two different channels: security and pulses. These channels recieve the subsequent messages depending on the event types. For testing purpose, I have directly embeded the slack's webhook api. This can be changed with the customised ones. Note that one has to change the WEN_HOOK_URL in slack.js with the cutomised webhooks for testing. 

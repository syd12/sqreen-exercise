/*
 Copyright Syed Gillani All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
      http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Syed Gillani
*/
let dbConfig = require("./log_config")
let l = require("../module/logging.js")

/**
This function uses the incoming messages in the body and add it to either security event collection
or the pulse event collection depending on the body
**/

let insertLog = function (body) {
    let bodycontent = JSON.parse(body)
    for (i = 0; i < bodycontent.length; i++) {
        if (bodycontent[i].sqreen_payload_type == undefined) {

            l.logger.error("sqreen_payload_type property is not defined")
            err= new Error('sqreen_payload_type property is not defined, issue parsing the Json file');
            return err //this.emit('error', err)
        }

        //parse the body to check which type of the event we are dearling with
 
        if (bodycontent[i].sqreen_payload_type == "security_event") {
           
            addSecurityEvents(bodycontent[i], function(err,data){

                if(err) return err
            })


          //  return addSecurityEvents(bodycontent[i])

        } else if (bodycontent[i].sqreen_payload_type == "pulse") {
           return addPulseEvents(bodycontent[i])
        }
    }

}

/** This function adds the security-related events in the security collection **/
let addSecurityEvents = function (body) {

    dbConfig.securityDB.insert(body, {
        w: 1
    }, function(err, result) {

        if (err) {
        	console.log(err)
            l.logger.error("Log insertion failed "+ err)
            return err
        }

        l.logger.info("added to the security events")


    });
}

/** This function adds the pulse-related events in the pulse collection **/

let addPulseEvents = function (body) {
    dbConfig.pulseDB.insert(body, {
        w: 1
    }, function(err, result) {

        if (err) {
            l.logger.error("Log insertion failed")
            return err
        }

        l.logger.info("added to the pulse events")

    });
}

module.exports = {
    version: '1.0',
    insertLog,
    addSecurityEvents,
    addPulseEvents
}
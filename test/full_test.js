/*
 Copyright Syed Gillani All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
      http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Syed Gillani
*/
let chai = require('chai');
let chaiHttp = require('chai-http');
let fs = require('fs');
chai.use(chaiHttp);
const should = chai.should();
var server = require('../src/server');

describe('/POST webhooks positive test', () => {
    it('it should  POST with correct header fields and json body and return 200', (done) => {
        let events = JSON.parse(fs.readFileSync('./test/body2.json', 'utf8'));
        chai.request(server)
            .post('/webhooks')
            .set('X-Sqreen-Integrity', '872e29f47a08508d04ddc1677258c9fb145bf73b858fb102d160976ec2c91d69')
            .send(events)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                done();
            });
    });

});



describe('/POST webhooks negative test', () => {
    it('it should not accept a POST with wrong header and returns 404 ', (done) => {
        let events = JSON.parse(fs.readFileSync('./test/body2.json', 'utf8'));
        chai.request(server)
            .post('/webhooks')
            .set('X-Sqreen-Integrity', '872e29f47a08508d04ddc1677258c9fb145bf73b858fb102d1')
            .send(events)
            .end((err, res) => {
                res.should.have.status(404)
                done();
            });
    });

});
/*
 Copyright Syed Gillani All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
      http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Syed Gillani
*/
const config = require('../config');
let slack = require('node-slackr');
let l = require("../module/logging.js").default

//a function to configure the security channel on the slack api
let security = new function() {
    if (config == undefined) {
        logger.fatal("Failed to extract the configuration...")
        return
    }
    return new slack(config.slack_config.apilink, {
        channel: "#" + config.slack_config.securityChannel,
    });
}
//a function to configure the pulse channel on the slack api
let pulses = new function() {
    if (config == undefined) {
        logger.fatal("Failed to extract the configuration...")
        return
    }
    return new slack(config.slack_config.apilink, {
        channel: "#" + config.slack_config.pulseChannel,
    });

}

module.exports = {
    version: '1.0',
    security,
    pulses
}
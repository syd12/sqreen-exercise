/*
 Copyright Syed Gillani All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
      http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Syed Gillani
*/
let assert = require('assert');
let log = require('../src/logs/logs.js')
let fs = require('fs');
let dbConfig = require("../src/logs/log_config")

//test the insertion and deletion of the security events
describe('Logging Security Events', function() {
    describe('Adding a row and then finding if it exists ', function() {
        it('should return the collection document with the application Id used from body.json', function() {
         
          var obj = JSON.parse(fs.readFileSync('./test/body.json', 'utf8'));
            //Add the new item
            log.addSecurityEvents(obj[0])
            //Check if it exists in the DB
            dbConfig.securityDB.findOne({
                application_id: '587b23384891d57c1bf5bf4f'
            }, function(err, item) {
                assert.equal(null, err);
                assert.equal('2016-12-21T07:22:32.732000+00:00', item.date_occurred);
            })

            //Remove it from the DB
            dbConfig.securityDB.remove({
                application_id: '587b23384891d57c1bf5bf4f'
            }, {
                w: 1
            }, function(err, item) {

                assert.equal(null, err);
                assert.equal('2016-12-21T07:22:32.732000+00:00', item.date_occurred);
            });


        });
    });




});
//test for the insertion and deletion of the pulse events
describe('Logging Pulse Events', function() {
    describe('Adding a row and then finding if it exists ', function() {
        it('should return the collection document with the application Id used from body.json', function() {
          var obj = JSON.parse(fs.readFileSync('./test/body.json', 'utf8'));
          //Add the new item
            log.addPulseEvents(obj[1])
            //check if it exists in the DB
            dbConfig.pulseDB.findOne({
                application_id: '587b23384891d57c1bf5bf4f'
            }, function(err, item) {

                assert.equal(null, err);

                assert.equal('2017-01-27T10:32:40.917000+00:00', item.date_ended);
            })
            //Remove it from the DB
            dbConfig.pulseDB.remove({
                application_id: '587b23384891d57c1bf5bf4f'
            }, {
                w: 1
            }, function(err, item) {
                assert.equal(null, err);
                assert.equal('2017-01-27T10:32:40.917000+00:00', item.date_ended);
            });


        });
    });

});
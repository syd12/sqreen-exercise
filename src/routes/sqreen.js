/*
 Copyright Syed Gillani All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
      http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Syed Gillani
*/
var router = require('express').Router();
var auth = require('../module/authentication.js')
var noti = require('../module/notifications')

router.post('/webhooks', function(req, res, next) {
    //Check the intregity and if true send the notifications to the targets 
    if (auth.checkIntegrity(req)) {
        noti.sendNotifications(new Buffer(req.rawBody, 'utf8'))
        res.statusCode = 200
        res.send('Notifications are sent to the targets!');
    } else {
        res.statusCode = 404
        res.send('The header key is not matched with the body signature');
    }

});

module.exports = router;
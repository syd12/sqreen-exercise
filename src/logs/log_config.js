/*
 Copyright Syed Gillani All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
      http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Syed Gillani
*/

const config = require('../config');
let slack = require('node-slackr');
let Db = require('tingodb')().Db;
let l = require("../module/logging.js").default
let db = new Db(process.env.PWD + '/src/logs/db', {});

//creating a TingoDB Collection used to store incoming security-related events
let securityDB = new function() {

    if (db == undefined) {
        l.logger.fatal("Cannot create or read the database files")
        return
    }

    return db.collection("security_events");

}
//creating a TingoDB collection used to store incoming pulse-related events
let pulseDB = new function() {

    if (db == undefined) {
        l.logger.fatal("Cannot create or read the database files")
        return
    }

    return db.collection("pulse_events");

}

module.exports = {
    version: '1.0',
    securityDB,
    pulseDB
}
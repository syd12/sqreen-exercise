/*
 Copyright Syed Gillani All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
      http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Syed Gillani
*/

const sever_config = {
  port: process.env.PORT ? process.env.PORT : 3000,
  baseUrl: process.env.BASE_URL ? process.env.BASE_URL : 'http://localhost:4000',
  cookieSecret: process.env.COOKIE_SECRET ? process.env.COOKIE_SECRET : 'StRoNGs3crE7',
  cookieName: process.env.COOKIE_NAME ? process.env.COOKIE_NAME : 'sqreen-session',
  sharedSecret: process.env.SHARED_SECRET ? process.env.SHARED_SECRET : 'super-secret',
  production: process.env.NODE_ENV ? process.env.NODE_ENV : 'production',
};
 
const slack_config = { 
     apilink:  process.env.SLACKAPI ? process.env.SLACKAI : 'https://hooks.slack.com/services/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', 
     securityChannel: process.env.SECURITY ? process.env.SECURITY : 'security',
     pulseChannel: process.env.PULSE ? process.env.PULSE : 'pulses',

  };

  const db_config = { 
     securityCollection:  process.env.SECURITYCOLL ? process.env.SECURITYCOLL  : 'security_events', 
     pulseCollection: process.env.PULSECOLL ? process.env.PULSECOLL : 'pulse_events',

  };
  


module.exports = {
  sever_config, 
  slack_config, 
  db_config
}

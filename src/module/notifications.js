/*
 Copyright Syed Gillani All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
      http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Syed Gillani
*/

let logs = require("../logs/logs.js");
let slack = require("../slack/slack_notifications.js");

//Interface to be used for multiple targets
let sendNotifications = function (body) {
    //record the incoming event in the log
    logs.insertLog(body, function(err) {
        if (err) {
            return err
        }
    });
    //send it to the slack
    slack.sendMessageToSlack(body, function(err) {
        if (err) {
            return err
        }
    });


}

module.exports = {
    version: '1.0',
    sendNotifications
}
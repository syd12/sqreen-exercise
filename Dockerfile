FROM node:carbon

WORKDIR /usr/sqreen-test/

COPY . .

RUN npm install

EXPOSE 3000

CMD [ "npm", "start" ]
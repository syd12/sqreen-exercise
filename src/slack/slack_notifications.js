/*
 Copyright Syed Gillani All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
      http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Syed Gillani
*/
const slack = require('./slack_config.js');
let l = require("../module/logging.js")


//Check what is the kind of incoming message

let sendMessageToSlack = function(body) {

    let bodycontent = JSON.parse(body)

    for (i = 0; i < bodycontent.length; i++) {


        if (bodycontent[i].sqreen_payload_type == undefined) {

            l.logger.error("The property (sqreen_payload_type) in the incoming message is not defined ")
            return new Error("The property (sqreen_payload_type) in the incoming message is not defined ")
        }

        if (bodycontent[i].sqreen_payload_type == "security_event")
            sendSecurityMessage(bodycontent[i], function(err) {
                if (err) return err
            })

        else if (bodycontent[i].sqreen_payload_type == "pulse")
            sendPulseMessage(bodycontent[i], function(err) {
                if (err) return err
            })

    }
}


//send the message to the security channel
let sendSecurityMessage = function(body) {

    let message = "Security alert at the " + body.application_name + ". The type of event is " + body.event_kind + ". More description is as follows: " + body.humanized_description
    slack.security.notify(message, function(err, result) {
        if (err) {
            l.logger.error(err, result);
            return err
        } else
            l.logger.info("Security message is sent to the slack")
    });
}

//send the message to the pulse channel
let sendPulseMessage = function(body) {

    let message = "Pulse alert at the " + body.application_name + " the type of event is " + body.humanized_title + ". More description is as follows: " + body.humanized_accounts + ". Pulse happend at location: " + body.humanized_geos

    slack.pulses.notify(message, function(err, result) {
        if (err) {
            l.logger.error(err, result);
            return err
        } else
            l.logger.info("Pulse message is sent to the slack")


    });

}

module.exports = {
    version: '1.0',
    sendMessageToSlack
}
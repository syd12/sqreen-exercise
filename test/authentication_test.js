/*
 Copyright Syed Gillani All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
      http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Syed Gillani
*/
let assert = require('assert');
let auth = require('../src/module/authentication.js')

// To test if the authentication with the header is taking place with correct signature. TODO: more detailed test on the recived Json file.
describe('Authentication', function() {
    describe('Compare the HMCcode for different bodies ', function() {
        it('should return true when the signature and body is the same as sent', function() {
            sharedSecret = "top-secret"
            body1 = "this is a body"
            signature1 = auth.createHMCodeForTesting(body1, sharedSecret)
            body2 = "this is a body"
            signature2 = auth.createHMCodeForTesting(body2, sharedSecret)
            assert.equal(signature1,  signature2 );
        });
    });

    // To test if the authentication with the header is taking place with incorrect signature. TODO: more detailed test on the recived Json file.
    describe('Negative Test ', function() {
        it('should return false since the body is compromised and is not what is expected', function() {

            sharedSecret = "top-secret"
            body1 = "this is a body"
            signature1 = auth.createHMCodeForTesting(body1, sharedSecret)
            body2 = "this is changed body"
            signature2 = auth.createHMCodeForTesting(body2, sharedSecret)
            compare =  signature1 == signature2
            assert.equal(false,  compare);
        });
    });



});
/*
 Copyright Syed Gillani All Rights Reserved.
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
      http://www.apache.org/licenses/LICENSE-2.0
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 Author: Syed Gillani
*/
/** This function takes the recived token from the header, the JSONfy string of the request's body, and the shared scerect 
 to check if the incoming request satisfy the intregity test **/
 const crypto = require("crypto");
 var fs = require('fs');
 var sharedSecret = require('../config').sever_config.sharedSecret;
 
 
 let getTokenFromHeader = function (req) {
     if (req.get("X-Sqreen-Integrity"))
         return req.get("X-Sqreen-Integrity");
     return null;
 }
 
 /**
 This function takes the recieved token, the recieved body and calculates the HMC coding. 
 The computed code is matched with the recived token to check the integrity of the recived message.
 **/
let checkIntegrity = function (req) { //recivedToken, bodystring, sharedSecret){
 
     recivedSignature = getTokenFromHeader(req)
     computedSignature = createHMCodeForTesting(new Buffer(req.rawBody, 'utf8'), sharedSecret)
     return computedSignature == recivedSignature 
 }
 
 /** The function that creates a new HMC code from input string**/
 let createHMCodeForTesting = function (body, sharedSecret) {
     hmac = crypto.createHmac("sha256", sharedSecret);
     hmac.write(body); // write in to the stream
     hmac.end(); // can't read from the stream until you call end()
     hash = hmac.read().toString('hex'); // read out hmac digest
     return hash //signature
 }
 
 
 module.exports = {
     version: '1.0',
     checkIntegrity,
     createHMCodeForTesting
 }